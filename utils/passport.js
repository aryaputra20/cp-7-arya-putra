const passport = require("passport");
const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");
const { User } = require("../models");

passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromHeader("authorization"),
      secretOrKey: "sangat-rahasia",
    },
    async (payload, done) => {
      console.log("PAYLOAD >>> ", payload);
      // get data user
      const user = await User.findByPk(payload.uid);
      done(null, user);
    }
  )
);

module.exports = passport;
