const { Room, Game } = require("../models");

exports.playGame = async (req, res) => {
  const room = await Room.findOne({
    where: {
      id: req.body.roomId,
    },
    include: Game,
  });
  if (!room) {
    res.status(404).send("Room not found");
    return;
  }

  const find = room.Games.find((item) => item.playerId === req.user.id);

  if (find) {
    res.status(500).send("User Sudah Join");
    return;
  }

  if (room.Games.length === 2) {
    res.status(500).send("Room Sudah Penuh");
    return;
  }

  const roomPlayer = await Game.create({
    roomId: req.body.roomId,
    playerId: req.user.id,
  });

  res.send(roomPlayer);
};
