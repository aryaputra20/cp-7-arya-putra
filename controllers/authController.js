const { User } = require("../models");
const jwt = require("jsonwebtoken");

exports.loginUser = async (req, res) => {
  try {
    const user = await User.findOne({
      where: {
        username: req.body.username,
        password: req.body.password,
      },
    });

    if (user) {
      const payload = { uid: user.id };
      const secret = "sangat-rahasia";

      const token = jwt.sign(payload, secret);
      res.json({
        token: token,
      });
    } else {
      res.status(401).send("Invalid login!");
    }
  } catch (err) {
    console.log(err);
    res.status(500).send(err.message);
  }
};

exports.registerUser = async (req, res) => {
  const user = await User.create({
    username: req.body.username,
    email: req.body.email,
    password: req.body.password,
  });
  res.send(user);
};
