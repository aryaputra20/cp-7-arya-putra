const express = require("express");
const router = express.Router();
const passport = require("../utils/passport");

const roomController = require("../controllers/roomController");

router.post(
  "/room",
  passport.authenticate("jwt", { session: false }),
  roomController.createRoom
);

module.exports = router;
