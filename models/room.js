"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    static associate(models) {
      models.Room.hasMany(models.Game, {
        foreignKey: "roomId",
      });
    }
  }
  Room.init(
    {
      winId: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      status: {
        type: DataTypes.ENUM(["OPEN", "IN_PROGRES", "CLOSED"]),
        allowNull: false,
        defaultValue: "OPEN",
      },
    },
    {
      sequelize,
      modelName: "Room",
    }
  );
  return Room;
};
